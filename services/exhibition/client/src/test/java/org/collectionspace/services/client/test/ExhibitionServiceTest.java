/**
 * This document is a part of the source code and related artifacts
 * for CollectionSpace, an open source collections management system
 * for museums and related institutions:
 *
 * http://www.collectionspace.org
 * http://wiki.collectionspace.org
 *
 * Copyright © 2009 Regents of the University of California
 *
 * Licensed under the Educational Community License (ECL), Version 2.0.
 * You may not use this file except in compliance with this License.
 *
 * You may obtain a copy of the ECL 2.0 License at
 * https://source.collectionspace.org/collection-space/LICENSE.txt
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.collectionspace.services.client.test;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.collectionspace.services.client.AbstractCommonListUtils;
import org.collectionspace.services.client.CollectionSpaceClient;
import org.collectionspace.services.client.ExhibitionClient;
import org.collectionspace.services.client.ExhibitionProxy;
import org.collectionspace.services.client.PayloadOutputPart;
import org.collectionspace.services.client.PoxPayloadIn;
import org.collectionspace.services.client.PoxPayloadOut;
import org.collectionspace.services.jaxb.AbstractCommonList;
import org.collectionspace.services.exhibition.ExhibitionsCommon;

import org.jboss.resteasy.client.ClientResponse;

import org.testng.Assert;
import org.testng.annotations.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ExhibitionServiceTest, carries out tests against a deployed and running Exhibition Service. <p/>
 * $LastChangedRevision:  $
 * $LastChangedDate:  $
 */
public class ExhibitionServiceTest extends AbstractPoxServiceTestImpl<AbstractCommonList, ExhibitionsCommon> {

    private final String CLASS_NAME = ExhibitionServiceTest.class.getName();
    private final Logger logger = LoggerFactory.getLogger(CLASS_NAME);
    final String SERVICE_PATH_COMPONENT = "exhibitions";
    private String knownResourceId = null;

    @Override
	public String getServicePathComponent() {
		return ExhibitionClient.SERVICE_PATH_COMPONENT;
	}

	@Override
	protected String getServiceName() {
		return ExhibitionClient.SERVICE_NAME;
	}
    
    @Override
    protected CollectionSpaceClient<AbstractCommonList, PoxPayloadOut, String, ExhibitionProxy> getClientInstance() {
        return new ExhibitionClient();
    }

    @Override
    protected AbstractCommonList getCommonList(ClientResponse<AbstractCommonList> response) {
        return response.getEntity(AbstractCommonList.class);
    }

    // ---------------------------------------------------------------
    // Utility methods used by tests above
    // ---------------------------------------------------------------
    
    @Override
    protected PoxPayloadOut createInstance(String identifier) {
    	ExhibitionClient client = new ExhibitionClient();
    	return createInstance(client.getCommonPartName(), identifier);
    }
    
	@Override
	protected PoxPayloadOut createInstance(String commonPartName,
			String identifier) {
		return createExhibitionInstance(identifier);
	}
    
    private PoxPayloadOut createExhibitionInstance(String uid) {
        String identifier = "title-" + uid;
        ExhibitionsCommon exhibition = new ExhibitionsCommon();
        exhibition.setExhibitionTitle(identifier);
      //  exhibition.setResponsibleDepartment("antiquities");
        PoxPayloadOut multipart = new PoxPayloadOut(ExhibitionClient.SERVICE_PAYLOAD_NAME);
        PayloadOutputPart commonPart = multipart.addPart(exhibition, MediaType.APPLICATION_XML_TYPE);
        commonPart.setLabel(new ExhibitionClient().getCommonPartName());

        if (logger.isDebugEnabled()) {
            logger.debug("to be created, exhibition common");
            logger.debug(objectAsXmlString(exhibition, ExhibitionsCommon.class));
        }

        return multipart;
    }

	@Override
	public void CRUDTests(String testName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected ExhibitionsCommon updateInstance(ExhibitionsCommon exhibitionsCommon) {
		ExhibitionsCommon result = new ExhibitionsCommon();
		
        result.setExhibitionTitle("updated-" + exhibitionsCommon.getExhibitionTitle());
		
		return result;
	}

	@Override
	protected void compareUpdatedInstances(ExhibitionsCommon original,
			ExhibitionsCommon updated) throws Exception {
		Assert.assertEquals(updated.getExhibitionTitle(), original.getExhibitionTitle());
	}
}
