/**
 * 
 */
package org.collectionspace.services;

public interface ExhibitionJAXBSchema {
    final static String EXHIBITION_TITLE = "title";
    final static String EXHIBITION_SCOPE_NOTE = "scopeNote";
    final static String EXHIBITION_RESPONSIBLE_DEPARTMENT = "responsibleDepartment";
}
